;;;; stumptray.asd
(asdf:defsystem #:stumpwm-translate			
  :serial t
  :description "Stumpwm translation utility"
  :author "Fermin MF"
  :license "GPLv3"
  :depends-on (:stumpwm :alexandria
	       :dexador :yason
	       :do-urlencode)
  :components ((:file "package")
               (:file "main")))

