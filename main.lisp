(in-package #:stumpwm-translate)


(defparameter *language-list*
  '(("spanish" "es")
    ("english" "en")
    ("detect" "auto")))

(defvar *translation-token* nil)

(defparameter *translation-format-url*
  "https://lingva.ml/_next/data/~a/~a/~a/~a.json")

(defun select-language (prompt)
  "Select a language from *language-list*."
  (let ((completions (mapcar #'car *language-list*)))
    (second
     (find (stumpwm:completing-read
	    (stumpwm:current-screen) prompt completions :require-match t)
	   *language-list* :key #'car  :test #'string=))))

(defun update-token ()
  "Update the translation token."
  (let* ((body (dexador:get "https://lingva.ml"))
	 (position (cl-ppcre:scan "buildManifest" body))
	 (token (subseq body (- position 23) (- position 2))))
    (when token
      (setf *translation-token* token)
      (stumpwm:message "Token updated!" token))))

(defun language-translate (from to value)
  (unless stumpwm-translate::*translation-token*
    (update-token))
  (gethash "translation"
	   (gethash
	    "pageProps"
	    (yason:parse
	     (dex:get
	      (format nil *translation-format-url*
		      stumpwm-translate::*translation-token*
		      from
		      to (do-urlencode:urlencode value)))))))

(stumpwm:defcommand translate-update-token () ()
  (update-token))

(stumpwm:defcommand translate () ()
  "Translate the current X selection into a selected language."
  (let* ((current-selection (stumpwm:get-x-selection))
	 (current-language (select-language "Current: "))
	 (target-language (select-language "Target: "))
	 (translation (language-translate current-language
					  target-language
					  current-selection)))
    (stumpwm:message "~a" translation)))
