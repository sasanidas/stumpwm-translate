;;;; package.lisp

(defpackage #:stumpwm-translate
  (:use #:cl #:alexandria)
  (:export #:translate
	   #:language-translate))

